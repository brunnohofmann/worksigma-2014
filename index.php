<?php
$cores = array(1 => "#EF7F46", 2=> "#4b6d93", 3=> "#54a76f");
$rand = rand(1, 3);
?><!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="ISO-8859-1" />
                <link rel="shortcut icon" id="favicon" type="image/x-icon" href="imgs/favicon.png">
		<title>Worksigma :: Pense diferente, crie algo novo e realize</title>
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		
		
		<script src="js/modernizr.custom.js"></script>
                <script src="js/jquery-1.11.1.min.js"></script>
                <link rel="stylesheet" href="js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
                <script type="text/javascript" src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body>
            <div class="row filete" ></div>
            <div class="row" >
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <img src="imgs/marca.png" style="width: 100%" alt="">
                    
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="row" >
                <div class="col-sm-7"></div>
                <div class="col-sm-3">
                    <span class="dias">De <b style="color: <?= $cores[$rand] ?>">28</b> de Julho a <b style="color: <?= $cores[$rand] ?>">01</b> de Agosto</span>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="row inscrevase">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <a class="btn btn-lg insc_bt<?= $rand ?> various fancybox.iframe" href="https://unisigma.typeform.com/to/lUPB2z">
                        Inscreva-se
                      </a>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="row descricao">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <p>A UniSigma Consultoria tem o prazer de lhe apresentar o Worksigma, evento promovido pela 
                        UniSigma Consultoria, empresa j�nior intercursos (Administra��o, Geoprocessamento e Sistemas 
                        para Internet) do Instituto Federal de Educa��o, Ci�ncia e Tecnologia da Para�ba- IFPB. </p>
                    <p>
	O  Worksigma est� na sua 4� edi��o e no corrente ano ser� realizado no per�odo de 28 de julho 
� 01 de agosto, no IFPB - campus Jo�o Pessoa. O objetivo � realizar atividades, minicursos e palestras com temas relevantes 
aos alunos, professores ou qualquer pessoa que tenha interesse em participar, procurando fomentar o esp�rito criativo e inovador em seus participantes.</p>
                    <p>Fa�a sua inscri��o clicando no bot�o acima ou presencialmente na sede da Unisigma Consultoria, localizada no IFPB - Campus Jo�o Pessoa. O ingresso custa apenas <b>R$15,00</b> e 
                        <b>1KG</b> de alimento n�o perec�vel. Com esse valor voc� participar dos 5 dias de evento, inclu�ndo Minicursos e Workshops. Garanta sua vaga no minicurso de seu interesse no momento do credenciamento, realizado no primeiro dia dedicado a cada �rea.</p>
                    <br>
                    <p><b>Para saber quem estar� presente no evento e os hor�rios correspondentes clique em uma das �reas abaixo.</b></p><br>
                </div>
                <div class="col-sm-2"></div>
            </div>
<!--            <div class="row apoio" >
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                    <h2 class="h1">Apoio:</h2>
                    <img src="imgs/apoio.png" class="img-rounded" />
                    
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-1">
                    <h2 class="h1">Realiza��o:</h2>
                    <img src="imgs/unisigma.png" class="img-rounded" />
                    
                </div>
                <div class="col-sm-2"></div>
            </div>-->
            <div class="clearfix"></div>
		<div class="row mosaico" >
                    <span class="nome_curso">Sistemas para<br>Internet<br><small>28 e 29 de julho</small></span>
                    <div class="cor" data-number='1' style="background-color: #e48857">
                        
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <h1 class="titulo_curso">Sistemas para Internet</h1><a class="close" href="#">X</a>
                                <ul class="nav_tab">
                                    <li><span data-link="palestras" data-area="tsi"><h1>Palestras</h1></span></li>
                                    <li><span data-link="workshop" data-area="tsi"><h1>Workshop</h1></span></li>
                                    <li><span data-link="minicurso" data-area="tsi"><h1>Minicursos</h1></span></li>
                                </ul>
                                
                            </div>                            
                        </div>
                        <div class="areas">
                            <div class="palestrastsi">
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Node.js</h1><h2 style="margin:0px 0px 20px 0px">com Manoel Quirino</h2></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/quirino.jpg" alt="Palestra de Node.js com Manoel Quirino"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>A palestra aborda o que � o node.js, quais benef�cios de us�-lo e mostra alguns cases de sucesso da tecnologia.</b></p>
                                                                <p>Manoel Quirino Neto � estudante de redes de computadores no IFPB e desenvolvedor h� cerca de 3 anos por voca��o e paix�o. Desenvolvedor do melhor site do Brasil no ano de 2013, participou de grandes projetos na Soda Virtual. Hoje trabalha na Arcos Brasil. </p>
                                                                <h3>Dia: <b>28</b><br>
                                                                    Hor�rio: <b>14:30</b></h3>
                                        </div>
                                        

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Desenvolvendo Aplica��es desktop javascript com node-webkit</h1> <h2 style="margin:0px 0px 20px 0px">com S�rgio Vilar</h2></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/sergio.jpg" alt="Palestra de Node-webkit com S�rgio Vilar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>A palestra mostra o que � o node-webkit, como ele funciona, quais suas vantagens, porque o PopcornTime resolveu us�-lo como plataforma e ao final temos um live-coding onde o palestrante ir� desenvolver um app semelhante ao Photo Booth do Mac OSX usando apenas HTML e Javascript.</b>
                                                            </p><p>S�rgio Vilar Lead Developer na Redesoft, Co-fundador do paraiba.js e graduando em An�lise e Desenvolvimento de Sistemas na Est�cio, trabalha hoje com Java, Backbone.js e Node.js. Tem experi�ncia tamb�m com PHP, Front-end e acessibilidade, no uso destas tecnologias agrega dois pr�mios: Pr�mio Nacional de Acessibilidade na Web 2012 promovido pela W3C Brasil e PHP Programming Innovation Award promovido pelo site PHP Classes.</p>
                                                             <h3>Dia: <b>28</b><br>
                                                                    Hor�rio: <b>15:00</b></h3>
                                        </div>

                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Palestra Chromecast e Android Wear</h1><h2 style="margin:0px 0px 20px 0px">com Silas Monteiro</h2></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/silas.jpg" alt="Representante da Google: Silas"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>O que � o Chromecast e o Android Wear, como funcionam e como desenvolver para eles.</b></p>
                                                                <p>Silas � desenvolvedor Web e Mobile. Co-Founder do LOFO (startup focada em pesquisa de marketing para o setor gastron�mico). Atualmente faz Engenharia da Computa��o na UFPB. � Organizer do GDG (Google Developers Group) e Manager da BlackBerry em Jo�o Pessoa.
            Ganhador do Space Apps Challenge NASA 2013, do Chrome Apps Challenge 2013 e do Startup JAM.</p>
                                                                 <h3>Dia: <b>28</b><br>
                                                                     Hor�rio: <b>17:00</b></h3>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>HACK, a nova linguagem desenvolvida pelo Facebook</h1><h2 style="margin:0px 0px 20px 0px">com Italo L�lis</h2></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/sila_monteiro.jpg" alt="Palestra de HACK com Italo L�lis"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>O que � HACK? O que � HHVM? Quais as diferen�as desta linguagem com o PHP? Que semelhan�as e diferen�as existem entre as duas linguagens? Essas e outras d�vidas ser�o sanadas com a palestra de Silas Monteiro.</b></p>
                                            <p>�talo L�lis � arquiteto de Software na Vox Tecnologia � graduado em An�lise e Desenvolvimento de Sistemas, j� trabalhou com tecnologias como VB6, VB.NET, C#, Java, Javascript, Node.js,
PHP entre outras. Atualmente cursa MBA em Engenharia de Software e possui uma ag�ncia de desenvolvimento web chamada Lellys Inform�tica, onde trabalha com PHP, HTML5, CSS3 e jQuery.</p>
                                             <h3>Dia: <b>29</b><br>
                                                                    Hor�rio: <b>15:45</b></h3>
                                        </div>

                                    </div>
                                <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A ser confirmada</h1></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profiletsi.jpg" alt=""></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>A ser confirmada.</b></p>
                                                                 <h3>Dia: <b>29</b><br>
                                                                     Hor�rio: <b>16:20</b></h3>
                                        </div>

                                    </div>
                                <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A ser confirmada</h1></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profiletsi2.jpg" alt=""></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A ser confirmada.</p>
                                             <h3>Dia: <b>29</b><br>
                                                                    Hor�rio: <b>17:00</b></h3>
                                        </div>

                                    </div>
                            </div>
                            <div class="workshoptsi" style="display:none">
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A ser confirmada</h1></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profiletsi.jpg" alt=""></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>A ser confirmada.</b></p>
                                                                 <h3>Dia: <b>29</b><br>
                                                                     Hor�rio: <b>16:20</b></h3>
                                        </div>

                                    </div>
                            </div>
                            <div class="minicursotsi"  style="display:none">
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Desenvolvendo Aplicativos para a Plataforma Android</h1><h2 style="margin:0px 0px 20px 0px">com Gustavo Soares</h2></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/gustavo.jpg" alt="Minicurso: Desenvolvendo Aplicativos para a Plataforma Android"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>Desde os primeiros passos para o desenvolvimento do seu pr�prio app at� a publica��o na Play Store... nosso primeiro convidado estar� presente na 4� edi��o do Worksigma compartilhando um pouco de sua experi�ncia.</b></p>
                                                                <p>Bacharel em Ci�ncia da Computa��o, com mais de 5 anos de experi�ncia no desenvolvimento de aplica��es para a plataforma Android. Atualmente � o respons�vel pelas aplica��es Android da SODA Virtual. Nas horas vagas � um apreciador de cerveja e entusiasta de novas tecnologias.</p>
                                                                <h3>Dias: <b>28</b> e <b>29</b><br>
                                                                    Hor�rio: <b>19:00</b> �s <b>21:00</b><br>
                                                                     Pr�-requisitos: <b>Conhecimento b�sico de Java</b></h3>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Desenvolvimento Front-End pregui�oso: automatizando tarefas e aumentando produtividade.</h1><h2 style="margin:0px 0px 20px 0px">com Arthur Gouveia</h2></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/arthur.jpg" alt="Minicurso: Desenvolvimento Front-End pregui�oso: automatizando tarefas e aumentando produtividade."></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>Aprenda mais sobre a import�ncia da pregui�a no desenvolvimento e crie um fluxo de trabalho automatizado, organizado e escal�vel, com a ajuda do Grunt, Sass + Compass, NPM e Bower.</b></p>
                                                                <p>Bacharel em Ci�ncias da Computa��o e focado em desenvolvimento Front-End,Arthur divide-se entre desenvolver aplica��es/websites tomando muito caf� e upar seus personagens de Guild Wars 2 enquanto aprecia diversos tipos de cerveja..</p>
                                                                 <h3>Dias: <b>28</b> e <b>29</b><br>
                                                                    Hor�rio: <b>19:00</b> �s <b>21:00</b><br>
                                                                     Pr�-requisitos: <b>Conhecimento b�sico de HTML, CSS e Javascript</b></h3>
                                        </div>

                                    </div>
                                <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Desenvolvendo Aplicativos para a Windows Phone</h1><h2 style="margin:0px 0px 20px 0px">com Allan Cleysson</h2></div>
                                    </div>
                                    <div class="row tsiatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/allan.jpg" alt="Minicurso: Desenvolvendo Aplicativos para Windows Phone"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>No minicurso de Desenvolvimento de Apps para Windows Phone 8 ser�o tratados assuntos relacionados � plataforma da Microsoft, ao longo do minicurso ser�o vistos os conceitos iniciais de Desenvolvimento e Designer, todos relacionados as boas pr�ticas recomendadas pela Microsoft evitando problemas no processo de avalia��o do seu aplicativo na Windows Store. Ser�o mostradas as duas principais formas de desenvolver aplica��es: Utilizando o Visual Studio e Utilizando o App Studio(ferramenta de prototipa��o e desenvolvimento de apps).</b></p>
                                                                <p>Allan Cleysson � Graduando em Engenharia de Computa��o pela Universidade Federal da Para�ba, desenvolve projetos na universidade junto ao LASER(Laborat�rio de Sistemas Embarcados e Rob�tica), em 2012 foi nomeado Microsoft Student Partner especializado na �rea de Desenvolvimento, tamb�m � colaborador no MIC-ETEPAM (Centro de Inova��o da Microsoft) desenvolvendo projetos relacionados a Web e Mobile.</p>
                                                                 <h3>Dias: <b>28</b> e <b>29</b><br>
                                                                     Hor�rio: <b>19:00</b> �s <b>21:00</b><br>
                                                                     Pr�-requisitos: <b>Conhecimento b�sico de Java ou C#</b>
                                        </div>

                                    </div>
                            </div>
                        </div>
                    </div>
                        
                        <div class="cor" data-number='2'style="background-color: #e3ab56"></div>
                        <span class="nome_curso">Administra��o<br><small>30 e 31 de julho</small></span>
                        <div class="cor" data-number='3' style="background-color: #4b6d93">
                            
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <h1 class="titulo_curso">Administra��o</h1><a class="close" href="#">X</a>
                                <ul class="nav_tab">
                                    <li><span data-link="palestras" data-area="adm"><h1>Palestras</h1></span></li>
                                    <li><span data-link="workshop" data-area="adm"><h1>Workshop</h1></span></li>
                                    <li><span data-link="minicurso" data-area="adm"><h1>Minicursos</h1></span></li>
                                </ul>
                               
                            </div>                            
                        </div>
                        <div class="areas">
                            <div class="palestrasadm">
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Cl�zio Amorim</h1></div>
                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/clezio.jpg" alt="Cl�zio Amorim    "></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"></p>
                                                                <p>Diretor da Planeta Rede Criativa, empresa de conex�o de mentes criativas e das marca BARA e Bela Pop, do setor de vestu�rio feminino. Facilitador das Conex�es em Criatividade, lideran�a compartilhada, desenvolvimento de equipes de alto desempenho e negocia��o com base no m�rito. Professor da UFPB. Graduado em Administra��o, Especialista em Exporta��o e Ecoturismo, Mestre em Administra��o, UFPR e Doutorando em Turismo e Desenvolvimento Sustent�vel, ULPGC, Espanha.</p>
                                                                <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>A confirmar</b></h3>
                                        </div>
                                        

                                    </div>
                                <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Case BeMais Supermercados</h1><h2 style="margin:0px 0px 20px 0px">com Ronaldo Cardoso</h2></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/ronaldo.jpg" alt="Ronaldo Cardoso"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>Ronaldo � Empres�rio e Administrador de Empresas, Membro da Associa��o Paraibana de Supermercados e Presidente do Grupo beMais Supermercados. Estara conosco no Worksigma compartilhando sua experiencia e falando do sucesso do BeMais SUpermercardos, que recentemente ganhou o premio TopOfMInd 2013.</p>
                                                             <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>14:50</b></h3>
                                        </div>

                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Movimento Choice</h1><h2 style="margin:0px 0px 20px 0px">com Mayara Lima</h2></div>
                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/mayara.jpg" alt="Movimento Choice com Mayara Lima"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>Voc� est� convidado a conferir de perto as realiza��es deste movimento numa ocasi�o onde os participantes ter�o a oportunidade de desenvolverem suas ideias em neg�cios sociais que impactem a sociedade de forma positiva.</b></p>
                                                                                          <p><b>O Movimento CHOICE � a maior rede de universit�rios engajados em neg�cios de impacto social do Brasil. Criado em 2011 pela ARTEMISIA para disseminar a discuss�o sobre o tema nas universidades. Escolas de neg�cios e tecnologia em todo o mundo- como Harvard (EUA), Stanford (EUA), Columbia (EUA), INSEAD (Fran�a), dentre outras- j� est�o engajadas com o tema de neg�cios de impacto social.</b></p>
                                                                <p>Mayara Lima � Embaixadora do Choice, estudante de Rela��es Internacionais da Universidade Federal da Para�ba e membro fundadora da L�deri J�nior, empresa j�nior de Rela��es Internacionais da UFPB.</p>
                                                                <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>16:10</b></h3>
                                        </div>
                                        

                                    </div>
                                 <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profileadm2.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>31 de julho</b><br>
                                                                    Hor�rio: <b>14:00</b></h3>
                                        </div>
                                    </div>
                                <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>Shirley Almeida</h1></div>
                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/shirley.jpg" alt="Shirley Almeida"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5">
                                                                                          <p><b>Medo de falar em p�blico? Conquistar as habilidades da orat�ria � um passo indispens�vel para o sucesso pessoal e profissional. Se voc� tem dificuldades para se apresentar, provavelmente est� perdendo oportunidades. � sobre isso que Shirley Almeida ir� abordar em sua palestra. A convidada � propriet�ria da Futura Cursos e Consultoria.</b></p>
                                                               
                                                                <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>16:00</b></h3>
                                        </div>
                                        

                                    </div>
                                <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profileadm.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>31 de julho</b><br>
                                                                    Hor�rio: <b>17:30</b></h3>
                                        </div>
                                    </div>
                                    

                                    
                            </div>
                            <div class="workshopadm" style="display:none">
                                     <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profileadm.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>15:00</b></h3>
                                        </div>
                                    </div>
                                 <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profileadm2.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>31 de julho</b><br>
                                                                    Hor�rio: <b>16:30</b></h3>
                                        </div>
                                    </div>
                            </div>
                            <div class="minicursoadm"  style="display:none">
                                     <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row admatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profileadm.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>30 de julho</b><br>
                                                                    Hor�rio: <b>16:30</b></h3>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </div>
                        <div class="cor" data-number='4' style="background-color: #547ba6"></div>
                        <span class="nome_curso">Geoprocessamento<br><small>01 de agosto</small></span>
                        <div class="cor" data-number='5' style="background-color: #54a76f">
                               
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <h1 class="titulo_curso">Geoprocessamento</h1><a class="close" href="#">X</a>
                                <ul class="nav_tab">
                                    <li><span data-link="palestras" data-area="geo"><h1>Palestras</h1></span></li>
                                    <li><span data-link="mesa" data-area="geo"><h1>Mesa Redonda</h1></span></li>
                                    <li><span data-link="minicurso" data-area="geo"><h1>Minicursos</h1></span></li>
                                </ul>
                               
                            </div>                            
                        </div>
                        <div class="areas">
                            <div class="palestrasgeo">
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>GIS e Web: Otimizando solu��es geogr�ficas</h1><h2 style="margin:0px 0px 20px 0px">com Talita Stael</h2></div>
                                    </div>
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/talita.jpg" alt="Talita Stael"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p><b>A Esri � uma empresa americana especializada na produ��o de solu��es para a �rea de informa��es geogr�ficas, sendo l�der mundial em sistemas de informa��o geogr�fica. Nossa ministrante, Talita Stael, ir� demostrar sobre programa��o no GIS para diversas ferramentas/aplicativos web, na tecnologia Esri, atrav�s de um de seus trabalhos utilizando o ArcObject, linguagem do ArcGIS.</b>
                                                            </p><p>� Bacharel em Ci�ncias da Computa��o pelo Unip� e Tecn�loga em Geoprocessamento pelo IFPB, S�cia-Fundadora da Athogeo. Possui ampla experi�ncia em Tecnologias ESRI no desenvolvimento de solu��es geogr�ficas desktop e webGIS e no desenvolvimento de plug-ins utilizando ArcObject, participou de grandes eventos nacionais e internacionais, como o Encontro de Usu�rios ESRI Brasil, e GeoMundus Conference on Geosciences, Geo-Information and Environment (2012), em Lisboa Portugal.</p>
                                                             <h3>Dia: <b>28</b><br>
                                                                    Hor�rio: <b>09:30</b></h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profilegeo2.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>01 de agosto</b><br>
                                                                    Hor�rio: <b>14:40</b></h3>
                                        </div>

                                    </div>
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profilegeo.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>01 de agosto</b><br>
                                                                    Hor�rio: <b>16:30</b></h3>
                                        </div>
                                    </div>
                                    

                                    
                            </div>
                            <div class="mesageo" style="display:none">
                                     <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profilegeo.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>01 de agosto</b><br>
                                                                    Hor�rio: <b>10:00</b></h3>
                                        </div>
                                    </div>
                            </div>
                            <div class="minicursogeo"  style="display:none">
                                     <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row geoatracoes">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profilegeo.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>01 de agosto</b><br>
                                                                    Hor�rio: <b>10:40 �s 12:00 / 14:00 �s 14:40</b></h3>
                                        </div>
                                    </div>
                                <div class="row ">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10"><h1>A Confirmar</h1></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2"><img src="imgs/atracoes/profilegeo2.jpg" alt="Atra��o a confirmar"></div>
                                        <div class="col-sm-1"></div><div class="col-sm-5"><p>A confirmar</p>
                                                             <h3>Dia: <b>01 de agosto</b><br>
                                                                    Hor�rio: <b>10:40 �s 12:00 / 14:00 �s 14:40</b></h3>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        
                        </div>
                        <div class="cor" data-number='6' style="background-color: #9cc75c"></div>
                </div>
            <div class="footer">
                <div class="container">
                    <div class="col-sm-10">
                    <h2>Apoio</h2>
                    <img src="imgs/apoio.png" usemap="#Map">
                    <map name="Map">
                      <area shape="rect" coords="17,24,152,86" href="http://ifpb.edu.br/" target="_blank" alt="IFpb">
                      <area shape="rect" coords="276,17,342,85" href="http://paraibajs.github.io/" target="_blank">
                      <area shape="rect" coords="352,20,418,88" href="http://php-pb.net/" target="_blank">
                      <area shape="rect" coords="429,26,649,83" href="http://gdgjp.org.br/" target="_blank">
                      <area shape="rect" coords="696,53,697,55" href="#">
                      <area shape="rect" coords="663,26,811,85" href="http://www.softcomtecnologia.com.br/" target="_blank">
                      <area shape="rect" coords="822,22,903,88" href="http://www.artemisia.org.br/conteudo/frentes/inspiracao/choice.aspx" target="_blank">
                      <area shape="rect" coords="912,23,1094,80" href="http://totcoworking.com.br/" target="_blank">
                      <area shape="rect" coords="1101,23,1163,89" href="https://www.facebook.com/vsetestudio" target="_blank">
                    </map>
                    </div>
                    <div class="col-sm-2">
                        <h2>Realiza��o</h2>
                        <a href="http://www.unisigma.com.br" target="_blank"><img src="imgs/uni.png"></a>
                    </div>
                </div>
            </div>
            
		<script src="js/svgcheckbx.js"></script>
	</body>
           <script src="js/style.js"></script>
           <script>
               $(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
           </script>
</html>