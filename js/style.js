$(".cor").click(function(){
    //checando e removendo classes gerais
    var numero = 0;
    var number = $(this).data('number');
        if (number % 2 === 0) number--;
        
    classemosaico();
    mosaico(number, numero);
    //$(".cor").css("height", ($(".ativa").height()/2))
    $('html, body').animate({
        scrollTop: $(".mosaico").offset().top
    }, 500);  
});

$(".detalhes").click(function(){
    
    $('html, body').animate({
        scrollTop: $(".mosaico").offset().top
    }, 500);  
    var numero = 0;
    var number = $(this).data('number');
        if (number % 2 === 0) number--;
        
    classemosaico();
    mosaico(number, numero);
    //$(".cor").css("height", ($(".ativa").height()/2))
    
   
});

function mosaico(number, numero){
    $(".mosaico .cor").each(function(){
        numero++;
        if(numero === number){
            $(this).addClass("ativa");
             
            $(".ativa>div>div>div.row").fadeIn( "fast", "linear" );
            $(".ativa .nav_tab").fadeIn( "fast", "linear" );
            $(".ativa .titulo_curso").fadeIn( "fast", "linear" );
            $(".ativa .nav_tab").css( "display", "inline-flex");
            $(".ativa .close").fadeIn( "fast", "linear" );
        }
        if ($(this).hasClass("ativa") == false){
            $(this).addClass("inativa");
            $(".inativa>div>div>div.row").fadeOut( "fast", "linear" );
            $(".inativa .nav_tab").fadeOut( "fast", "linear" );
            $(".inativa .titulo_curso").fadeOut( "fast", "linear" );
            $(".inativa .close").fadeOut( "fast", "linear" );
         }
    });
}

function classemosaico(){
    $(".mosaico .cor").each(function(){
            $(this).removeClass("ativa");
            $(this).removeClass("inativa");
    });
     $(".nome_curso").fadeOut( "fast", "linear" );
}

$(document).ready(function(){
   $(window).scroll(function () {
       if ($(this).scrollTop() <= 100){
         $(".mosaico .cor").each(function(){
             $(".nome_curso").css( "display", "inline" );
            $(".titulo_curso").fadeOut( "fast", "linear" );
            $(".cor>div>div>div.row").fadeOut( "fast", "linear" );
            $(".nav_tab").fadeOut( "fast", "linear" );
            $(".close").fadeOut( "fast", "linear" );
            $(this).removeClass("ativa");
            $(this).removeClass("inativa");
        });  
       }
   }); 
});



$(".close").click(function(){
     $('html, body').animate({
        scrollTop: $(".filete").offset().top
    }, 500);  
});

$(".nav_tab li span").click(function(){
    $(".nav_tab li span").each(function(){
       $(this).css("text-decoration", "none");
    });
    var link = $(this).data("link")+$(this).data("area");
    $(this).css("text-decoration", "underline");
    $(".ativa>div.areas>div").each(function(){
        if($(this).hasClass(link)){
            $(this).fadeIn( "fast", "linear");
        }else{
             $(this).fadeOut( "fast", "linear");
        }  
    });
});