<?php
 	include 'conn.php';
 	session_start();

	$name = $_POST['q1'];
	$rg = $_POST['q2'];
	$mat = $_POST['q3'];
	$email = $_POST['q4'];
	$tel = $_POST['q5'];
	$idade = $_POST['q6'];

	try{
		$env = $conn -> prepare('INSERT INTO cs (nome, rg, matricula, email, telefone, data_nasc) VALUES (:nome, :rg,:matricula,:email,:telefone,:idade)');
		$env -> execute(array( ':nome' => $name,
						':rg' => $rg,
						':matricula' => $mat,
						':email' => $email,
						':telefone' => $tel,
						':idade' => $idade));

		$_SESSION['finish'] = 'finish';

	}catch(PDOException $e){
		echo $e->getMessage();
		$_SESSION['finish'] = 'noFinish';
	}

	header('location: ../index.php');

?>