<?php
	session_start();
	// session_destroy();
?>
<!DOCTYPE html>
<html lang="pt-br" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Campeonato de Counter Strike</title>
		<meta name="description" content="Pagina de inscrição para o campionato de cs no worksigma" />
		<meta name="keywords" content="form, minimal, interface, single input, big form, responsive form, transition" />
		<meta name="author" content="Codrops" />
                <link rel="shortcut icon" id="favicon" type="image/x-icon" href="img/cs-logo.png">
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<link rel="shortcut icon" type='image/png' href="favicon.png"> 	
		<script src="js/jquery.js"></script>
		<script src="js/modernizr.custom.js"></script>
	</head>
	<body>
		<audio autoplay class='kill-sound'>
			<source src='sons/entrada-wlktak.mp3' type='audio/mp3'>
			<source src='sons/entrada-wlktak.wav' type='audio/wav'>
		</audio>
		<p class='title_camp'>Inscrição do Campeonato de Counter Strike</p>
		<div class="container">
			<section>
				<!-- <img src="img/cs.png"> -->
					<form id="theForm" name='form_insc' class="simform" method='post' action='php/get-info.php' autocomplete="off">
						<div class="simform-inner">
							<ol class="questions">
								<li>
									<span><label for="q1">Nome*</label></span>
									<input id="q1" name="q1" type="text" value='joerverson'/>
								</li>
								<li>
									<span><label for="q2">RG*</label></span>
									<input id="q2" name="q2" type="text" value='0980988'/>
								</li>
								<li>
									<span><label for="q3">Matrícula (se não tiver pressione ENTER)*</label></span>
									<input id="q3" name="q3" type="text" value=''/>
								</li>
								<li>
									<span><label for="q4">Email*</label></span>
									<input id="q4" name="q4" type="text" value='teste@g.g'/>
								</li>
								<li>
									<span><label for="q5">Telefone*</label></span>
									<input id="q5" name="q5" type="text" value='(12)12121212'/>
								</li>
								<li>
									<span><label for="q6">Data de Nascimento*</label></span>
									<input id="q6" name="q6" type="text" value='09-00-0000'/>
								</li>
							</ol><!-- /questions -->
							<button class="submit" type="submit">Send answers</button>
							<div class="controls">
								<button class="next"></button>
								<div class="progress"></div>
								<span class="number">
									<span class="number-current"></span>
									<span class="number-total"></span>
								</span>
								<span class="error-message"></span>
							</div><!-- / controls -->
						</div><!-- /simform-inner -->
						<div class="final-message"></div>
					</form><!-- /simform -->	
					<div class="final-message"></div>	
					<input type='button' value='edital'>	
				</section>
		</div><!-- /container -->
		<footer>
			<p>
				Realização: UniSigma Consultoria e CATSI IFPB.
			<br>Apoio: IFPB - Campus João Pessoa.
			</p>
		</footer>
		<script src="js/classie.js"></script>
		<script src="js/stepsForm.js"></script>
		<script>
			var theForm = document.getElementById( 'theForm' );

				new stepsForm( theForm, {
					onSubmit : function( form ) {
						// hide form

						classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );
						form.submit();						

						classie.addClass( messageEl, 'show' );
						
					}
				} );
		</script>
		<?php
			if($_SESSION['finish'] == 'finish'){
		?>
			<script type="text/javascript">
				$('.kill-sound').remove();

				$('.simform-inner').hide();
				$('.simform').css({margin:'2% auto'});
				$('.final-message').append('Formulario Enviado com Sucesso!').css({zIndex:'100', diplay:'block', opacity:'1 !important'});
				setTimeout(function(){ // adicionando o ultimo audio
					$('body').append("<audio autoplay class='kill-sound'><source src='sons/fin.mp3' type='audio/mp3'><source src='sons/fin.wav' type='audio/wav'></audio>");
				},1000);
				
				setTimeout(function(){ // removendo todos os audios
					$('.kill-sound').remove();
				},7000);
			</script>
		<?php
			}
		?>
	</body>
</html>